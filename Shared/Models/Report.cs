using System;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models
{
    public class Report
    {
        public Report()
        {
        }

        public Report(Guid fileId, string fileName, Guid taskId, bool result, DateTime date)
        {
            Id = Guid.NewGuid();
            FileId = fileId;
            FileName = fileName;
            TaskId = taskId;
            Result = result;
            Date = date;
        }

        [Key] public Guid Id { get; }
        public Guid FileId { get; }
        public string FileName { get; }
        public Guid TaskId { get; }
        public bool Result { get; }
        public DateTime Date { get; }
    }
}