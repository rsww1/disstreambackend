﻿using Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler.MessageHandlers
{
    public class AnalyzeFileHandler : IHandler<AnalyseFileCommand>
    {
        public Task HandleAsync(AnalyseFileCommand message, CancellationToken token)
        {
            Console.WriteLine($"Received file to analysis : {message.Id}");
            return Task.CompletedTask;
        }
    }
}
