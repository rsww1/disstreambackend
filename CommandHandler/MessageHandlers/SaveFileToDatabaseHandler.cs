﻿using Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler.MessageHandlers
{
    public class SaveFileToDatabaseHandler : IHandler<SaveFileToDatabaseCommand>
    {
        public async Task HandleAsync(SaveFileToDatabaseCommand message, CancellationToken token)
        {
            Console.WriteLine($"Received {message.FileId}");
            return ;
        }
    }
}
