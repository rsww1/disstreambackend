﻿using Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler.MessageHandlers
{
    public interface IHandler<in T> where T : Command
    {
        Task HandleAsync(T message, CancellationToken token);
    }
}
