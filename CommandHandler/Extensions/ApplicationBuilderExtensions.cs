﻿using CommandHandler.MessageHandlers;
using Commands;
using Microsoft.AspNetCore.Builder;
using RawRabbit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder AddHandler<T>(this IApplicationBuilder app, IBusClient client)
       where T : Command
        {
            if (!(app.ApplicationServices.GetService(typeof(IHandler<T>)) is IHandler<T> handler))
                throw new NullReferenceException();

            client
                .SubscribeAsync<T>(async (msg, context) =>
                {
                    await handler.HandleAsync(msg, CancellationToken.None);
                });
            return app;
        }
        public static IApplicationBuilder AddHandler<T>(this IApplicationBuilder app)
            where T : Command
        {
            if (!(app.ApplicationServices.GetService(typeof(IBusClient)) is IBusClient busClient))
                throw new NullReferenceException();

            return AddHandler<T>(app, busClient);
        }

    }
}
