﻿using System;

namespace Queries
{
    public record ProcessFile (Guid Id, Guid UserId, string FileName, string Status, DateTime DateTime)
    {
    }
}