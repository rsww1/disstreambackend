using System.Collections.Generic;
using LoginService.Dto;
using LoginService.Models;

namespace LoginService.Services
{
    public interface ILoginService
    {
        User CreateUser(string login, string password);
        string Login(LoginRequest request);
    }
}