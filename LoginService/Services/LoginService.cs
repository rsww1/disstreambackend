using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using LoginService.Database;
using LoginService.Dto;
using LoginService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace LoginService.Services
{
    public class LoginService : ILoginService
    {
        private readonly UserContext _userContext;
        private readonly IConfiguration _configuration;
        
        public LoginService(UserContext userContext, IConfiguration configuration)
        {
            _userContext = userContext;
            _configuration = configuration;
        }
        
        public User CreateUser(string login, string password)
        {
            CheckIfUserExists(login);
            var user = new User(login, password);
            _userContext.Users.Add(user);
            _userContext.SaveChanges();
            return user;
        }

        public string Login(LoginRequest request)
        {
            var user = AuthenticateUser(request.Login, request.Password);
            return user != null ? CreateJwtForUser(user) : null;
        }
        
        private void CheckIfUserExists(string login)
        {
            if (_userContext.Users.FirstOrDefault(user => user.Login.Equals(login)) != null)
            {
                throw new DuplicateNameException("Login taken");
            }
        }

        private string CreateJwtForUser(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                CreateClaimsForUser(user),
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: credentials);
            
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private User AuthenticateUser(string login, string password)
        {
            return _userContext.Users.FirstOrDefault(user =>
                user.Login.Equals(login) && user.Password.Equals(password));
            return new User("login", "password");
        }

        private static IEnumerable<Claim> CreateClaimsForUser(User user)
        {
            return new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim("login", user.Login)
            };
        }
    }
}