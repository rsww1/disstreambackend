using LoginService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoginService.Database
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options) { }
        
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            SetupBuilder(modelBuilder.Entity<User>());
        }

        private void SetupBuilder(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.ToTable("users");
            builder.Property(u => u.Id).HasColumnName("id");
            builder.Property(u => u.Login).HasColumnName("login");
            builder.Property(u => u.Password).HasColumnName("password");
        }
    }
}