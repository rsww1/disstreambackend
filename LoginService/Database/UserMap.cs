using LoginService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LoginService.Database
{
    public class UserMap
    {
        public UserMap(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.ToTable("users");
            builder.Property(u => u.Id).HasColumnName("id");
            builder.Property(u => u.Login).HasColumnName("login");
            builder.Property(u => u.Password).HasColumnName("password");
        }
    }
}