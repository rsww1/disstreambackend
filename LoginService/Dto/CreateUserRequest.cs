namespace LoginService.Dto
{
    public record CreateUserRequest(string Username, string Password) { }
}