using System;
using System.ComponentModel.DataAnnotations;

namespace LoginService.Models
{
    public class User
    {
        public User() { }

        public User(string login, string password)
        {
            Login = login;
            Password = password;
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; }
        public string Login { get; }
        public string Password { get; }
    }
}