using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Common.Utils
{
    public class JwtUtil
    {
        public static Token GetUserDataFromToken(string token)
        {
            if(string.IsNullOrEmpty(token)) throw new UnauthorizedAccessException();
            
            token = RemoveBearer(token);
            
            var stringToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var claims = (List<Claim>) stringToken.Claims;

            return new Token()
            {
                Id = Guid.Parse(claims.First(c => c.Type.Equals("sub")).Value),
                Login = claims.First(c => c.Type.Equals("login")).Value
            };
        }

        private static string RemoveBearer(string token)
        {
            const string bearer = "Bearer ";
            return token.Contains(bearer) ? token.Substring(bearer.Length) : token;
        }
    }
}