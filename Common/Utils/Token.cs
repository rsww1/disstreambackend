using System;

namespace Common.Utils
{
    public class Token
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
    }
}