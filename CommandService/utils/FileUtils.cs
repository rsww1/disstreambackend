using System.IO;
using Commands;
using Microsoft.AspNetCore.Http;

namespace CommandService.utils
{
    public static class FileUtils
    {
        public static FileWrapper CreateFileWrapper(IFormFile file)
        {
            var memoryStream = new MemoryStream();
            file.CopyTo(memoryStream);
            return new FileWrapper(file.FileName, memoryStream.ToArray());
        }
    }
}