using System;
using System.Threading.Tasks;
using Commands;
using RawRabbit;

namespace CommandService.Services
{
    public class ProcessService : IProcessService
    {
        private readonly IBusClient _client;

        public ProcessService(IBusClient client)
        {
            _client = client;
        }
        
        public async Task<Guid> ProcessFile(Guid fileId, Guid userId)
        {
            var taskId = Guid.NewGuid();
            await _client.PublishAsync(new ProcessFileCommand(fileId, taskId, userId));
            return taskId;
        }
    }
}