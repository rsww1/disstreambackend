using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CommandService.Services
{
    public interface IFileService
    {
        public Task<Guid> SaveFile(IFormFile file, Guid userId);
    }
}