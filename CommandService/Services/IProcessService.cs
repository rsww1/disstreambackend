using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CommandService.Services
{
    public interface IProcessService
    {
        Task<Guid> ProcessFile(Guid fileId, Guid userId);
    }
}