using System;
using System.Threading.Tasks;
using Commands;
using CommandService.utils;
using Microsoft.AspNetCore.Http;
using RawRabbit;

namespace CommandService.Services
{
    public class FileService : IFileService
    {
        private readonly IBusClient _busClient;

        public FileService(IBusClient busClient)
        {
            _busClient = busClient;
        }

        public async Task<Guid> SaveFile(IFormFile file, Guid userId)
        {
            var fileId = Guid.NewGuid();
            var taskId = Guid.NewGuid();
            var fileWrapper = FileUtils.CreateFileWrapper(file);
            Console.WriteLine($"SENDING FILE : {fileId}");
            await _busClient.PublishAsync(new SaveFileToDatabaseCommand(fileId, taskId, userId, fileWrapper));
            return fileId;

        }
    }
}
