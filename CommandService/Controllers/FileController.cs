using System;
using System.Threading.Tasks;
using CommandService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CommandService.Controllers
{
    [ApiController]
    [Route("api/file")]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpPost("save")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> SaveFile(IFormFile file, [FromHeader] string authorization) //TODO add authorization
        {
            var userId = Guid.NewGuid(); //TODO get user id based on jwt 
            Console.WriteLine(authorization);
            Console.WriteLine(file.FileName);
            Console.WriteLine(file.Length);
            var fileId = await _fileService.SaveFile(file, userId);
            return new AcceptedResult($"api/file/{fileId}", fileId);
        }
    }
}