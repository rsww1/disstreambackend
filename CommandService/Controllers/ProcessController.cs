using System;
using System.Threading.Tasks;
using CommandService.Services;
using Microsoft.AspNetCore.Mvc;

namespace CommandService.Controllers
{
    [ApiController]
    [Route("api/process")]
    public class ProcessController : Controller
    {
        private readonly IProcessService _processService;

        public ProcessController(IProcessService processService)
        {
            _processService = processService;
        }

        [HttpPost("start")]
        public async Task<IActionResult> ProcessFile([FromHeader] string authorization)
        {
            var fileId = Guid.Parse(Request.Form["fileId"]);
            Console.WriteLine(authorization);
            var userId = Guid.NewGuid(); //TODO get user id based on jwt 
            var taskId = await _processService.ProcessFile(fileId, userId);
            return new AcceptedResult($"api/process/{taskId}", taskId);
        }
    }
}