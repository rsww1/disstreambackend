using System;

namespace Commands
{
    public record SaveFileToDatabaseCommand(Guid FileId, Guid TaskId, Guid UserId, FileWrapper File) : Command(TaskId, UserId)
    {
    }
}