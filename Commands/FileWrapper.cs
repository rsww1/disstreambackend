﻿using System;
using System.Collections;

namespace Commands
{
    public record FileWrapper (string Name, IEnumerable Content)
    {
    }
}