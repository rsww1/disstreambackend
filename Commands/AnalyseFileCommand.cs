﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commands
{
    public record AnalyseFileCommand(Guid Id, Guid UserId) : Command(Id, UserId)
    {

    }
}
