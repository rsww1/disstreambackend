using System;

namespace Commands
{
    public record ProcessFileCommand(Guid FileId, Guid TaskId, Guid UserId) : Command(TaskId, UserId)
    {
        
    }
}