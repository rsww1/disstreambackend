using System;

namespace Commands
{
    public abstract record Command(Guid TaskId, Guid UserId)
    {
        
    }
}