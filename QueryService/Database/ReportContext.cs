using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Models;


namespace QueryService.Database
{
    public class ReportContext  : DbContext
    {
        public ReportContext(DbContextOptions<ReportContext> options) : base(options) { }
        
        public DbSet<Report> Reports { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            SetupBuilder(modelBuilder.Entity<Report>());
        }

        private void SetupBuilder(EntityTypeBuilder<Report> builder)
        {
            builder.HasKey(report => report.Id);
            builder.ToTable("reports");
            builder.Property(report => report.Id).HasColumnName("id");
            builder.Property(report => report.FileId).HasColumnName("file_id");
            builder.Property(report => report.FileName).HasColumnName("file_name");
            builder.Property(report => report.TaskId).HasColumnName("task_id");
            builder.Property(report => report.Result).HasColumnName("is_plagiarism");
            builder.Property(report => report.Date).HasColumnName("process_date");
        }
    }
}