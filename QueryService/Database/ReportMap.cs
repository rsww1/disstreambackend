using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Models;

namespace QueryService.Database
{
    public class ReportMap
    {
        public ReportMap(EntityTypeBuilder<Report> builder)
        {
            builder.HasKey(report => report.Id);
            builder.ToTable("Reports");
            builder.Property(report => report.Id).HasColumnName("Id");
            builder.Property(report => report.FileId).HasColumnName("file_id");
            builder.Property(report => report.FileName).HasColumnName("file_name");
            builder.Property(report => report.TaskId).HasColumnName("task_id");
            builder.Property(report => report.Result).HasColumnName("is_plagiarism");
            builder.Property(report => report.Date).HasColumnName("process_date");
        }
    }
}