using System;
using System.Collections.Generic;
using Queries;

namespace QueryService.Services
{
    public interface IFileService
    {
        ProcessFile GetProcessFileById(Guid id);
        List<ProcessFile> GetAllProcessFiles();
    }
}