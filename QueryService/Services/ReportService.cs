using System;
using System.Linq;
using QueryService.Database;
using Shared.Models;

namespace QueryService.Services
{
    public class ReportService : IReportService
    {
        private readonly ReportContext _reportContext;

        public ReportService(ReportContext reportContext)
        {
            _reportContext = reportContext;
        }
        
        public Report GetReportByFileId(Guid fileId)
        {
            return _reportContext.Reports.FirstOrDefault(report => report.FileId == fileId);
        }
    }
}