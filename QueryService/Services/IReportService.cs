using System;
using Shared.Models;

namespace QueryService.Services
{
    public interface IReportService
    {
        Report GetReportByFileId(Guid fileId);
    }
}