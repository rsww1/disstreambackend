using System;
using System.Collections.Generic;
using Queries;

namespace QueryService.Services
{
    public class FileService : IFileService
    {
        public ProcessFile GetProcessFileById(Guid id)
        {
            return new ProcessFile(Guid.NewGuid(), Guid.NewGuid(), "name.cs", "waiting", DateTime.Now);
        }

        public List<ProcessFile> GetAllProcessFiles()
        {
            var pf1 = new ProcessFile(Guid.NewGuid(), Guid.NewGuid(), "name1.cs", "waiting", DateTime.Now);
            var pf2 = new ProcessFile(Guid.NewGuid(), Guid.NewGuid(), "name2.cs", "running", DateTime.Now);
            var pf3 = new ProcessFile(Guid.Parse("d2ee457f-c3d0-43de-a425-169da07f76eb"), Guid.NewGuid(), "name3.cs", "completed", DateTime.Now);

            return new List<ProcessFile>() {pf1, pf2, pf3};
        }
    }
}