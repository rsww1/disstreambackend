using System;
using Microsoft.AspNetCore.Mvc;
using QueryService.Services;
using QueryService.Utils;

namespace QueryService.Controllers
{
    [ApiController]
    [Route("api/report")]
    public class ReportController : Controller
    {
        private readonly IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }
        
        [HttpGet("get")]
        [Produces("application/pdf")]
        public IActionResult GetPlagiarismReportById([FromQuery] Guid fileId, [FromHeader] string authorization)
        {
            var report = _reportService.GetReportByFileId(fileId);
            Console.WriteLine(DateTime.Now);
            Console.WriteLine(report.Result);
            Console.WriteLine(report.FileId);
            Console.WriteLine(report.Date);
            var pdf = ReportGenerator.GenerateReport(report);
            return File(pdf, "application/pdf");
        }
    }
}