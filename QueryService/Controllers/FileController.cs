using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Queries;
using QueryService.Services;

namespace QueryService.Controllers
{
    [ApiController]
    [Route("api/sourcefile")]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet("{id}")]
        [Produces("application/json")]
        public IActionResult GetProcessFileById([FromRoute] Guid id)
        {
            var processFile = _fileService.GetProcessFileById(id);
            return new OkObjectResult(processFile);
        }

        [HttpGet("all")]
        [Produces("application/json")]
        public IActionResult GetAllProcessFiles()
        {
            var processFiles = _fileService.GetAllProcessFiles();
            return new OkObjectResult(processFiles);
        }
    }
}