using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Shared.Models;

namespace QueryService.Utils
{
    public static class ReportGenerator
    {
        public static byte[] GenerateReport(Report report)
        {
            using var memoryStream = new System.IO.MemoryStream();
            var document = new Document();
            var writer = PdfWriter.GetInstance(document, memoryStream);
            writer.CloseStream = false;
            document.Open();
            document.AddTitle($"Plagiarism report for file {report.FileName}");
            AddLine(document, "Filename: ", report.FileName);
            AddLine(document, "File id: ", report.FileId.ToString());
            AddLine(document, "Process date: ", report.Date.ToString(CultureInfo.InvariantCulture));
            AddLine(document, "Task id: ", report.TaskId.ToString());
            AddLine(document, "Is a plagiarism: ", report.Result.ToString());
            document.Close();
            var asBytes = memoryStream.ToArray();
            return asBytes;
        }

        private static void AddLine(IElementListener document, string key, string value)
        {
            var line = new Paragraph($"{key} {value}");
            document.Add(line);
        }
    }
}